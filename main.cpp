/*Brian Cabral 
this progran will display the height of the tree
the user will define amount of nodes and node values
*/
#include <iostream>
#include <algorithm>
#include <string>
#include <sstream>

/*Node Struct.
 * user defined date.
 * points to left and right
 * node */
struct Node{
	int data;
	Node *left;
	Node *right;
};


//function declartions
Node* newNode(int data);
Node *insertNode(Node *root, int data);
void printTree(Node *root, int nodes);
int maxDepth(Node *root);
int checkPostiveNumber(int *nodeData);
int stringToInt(std::string s);
void checkNode(int *nodeValue);

//start of main
int main(){
	//initilized integerss in stack memory
	int nodeCount,nodeValue;
	//initilze BST node in heap memory
	Node* BSTNode=new Node();
	//set BSTNode to Null
	BSTNode=NULL;
	//while loop asking for amount of nodes. will keep looping 
	//until postive integer
	while(checkPostiveNumber(&nodeCount));
	//for loop populating the nodes of the tree.
	for(int i=0;i<nodeCount;i++){
		std::cout<<"Enter value of node "<<i+1<<",\n";
		checkNode(&nodeValue);
		BSTNode=insertNode(BSTNode,nodeValue);
	}
	//print tree to confirm height is correct
	printTree(BSTNode,nodeCount);
	//preset answer to user maxdepth function
	std::cout<<"\nHeight of tree: "<<maxDepth(BSTNode)<<std::endl;
	//delete BSTNode from Memory and set to null
	delete(BSTNode);
	BSTNode=NULL;

	return 0;
}

/*new node function
 * returns a new node with the left and right pointer pointing to null
 * data will be usserdefined*/
Node* newNode(int data){
	//allocate new node in heap memory
	Node* newNode=new Node();
	newNode->data=data;
	newNode->left=NULL;
	newNode->right=NULL;
	
	return newNode;
}
/*Insert node function
 * returns node. pass in BSTNode and user defined data.
 * recursive function
 */ 
Node *insertNode(Node *root,int data){
	//base case, if root is null then make the new node and insert it
	if(root==NULL)
		root=newNode(data);
	//if data iss lessss then root node data, then insert to right of root
	else if(root->data>=data)
		root->left=insertNode(root->left,data);
	//if data of node isss more then root node data, then insert it to the rright of root
	else
		root->right=insertNode(root->right,data);
	return root;
}
/*print tree function 
 * prints the tree in a sideways graphical way
 * just meant to confirm height is correct
 */
void printTree(Node *root,int nodes){
	//initilize spacing to nodes. this iss so the tree is not smashed together
	int spacing=nodes;
	//base case when root has nothing. return nothing
	if(root==NULL)
		return;
	nodes +=spacing;
	//recursively print right nodes
	printTree(root->right,nodes);
	std::cout<<std::endl;
	for(int i=spacing;i<nodes;i++)
		std::cout<<" ";
	//print data
	std::cout<<root->data<<"\n";
	//recursively print left nodes
	printTree(root->left,nodes);
}
/* returns the height of the tree
 * recursive function
 */
int maxDepth(Node *root){
	//base case: return -1. makes it so if there iss only one roote nood it will cause the height to be 0;
	if(root==NULL)
		return -1;
	//recursive call that adds one everytime it passes through the function.
	//takes max of left or right tree and returns the result
	return (std::max(maxDepth(root->left),maxDepth(root->right))+1);
}

//verify user input is valid and positive to make nodes
int checkPostiveNumber(int* nodeData){
	//create string for userInput
	std::string userInput;
	//boolean flag
	bool valid =true;
	std::cout<<"How many nodes?\n";
	std::cin>>userInput;
	//range base loop creating char data types out of sstring
	//for(char const &c:userInput){ version of g++ does not have this yet.
	//made into itterative
	for(int i=0;i<userInput.size();i++){
		char c=userInput[i];
		if(isdigit(c)){//if it is an actual digit then continue
			continue;
		}else{//else ask again
			valid=false;
			break;
		}
	}if(valid){
		*nodeData=stringToInt(userInput);
		//must be postive amount of nodes
		if(*nodeData>=1){
			return EXIT_SUCCESS;
		}else{//else ask again.
			return EXIT_FAILURE;
		}
	}else{
		std::cout<<"Please enter a postive integer"<<std::endl;
		return EXIT_FAILURE;
	}
}

//function to make string into an integer
int stringToInt(std::string s){
	//initilized int
	int i=0,sum=0;
	//while string is not empty
	while(s[i]!='\0'){
		//if it iss not between the ascii values it can not be converted
		//else convert to integer
		if (s[i]<48||s[i]>57){
			std::cout<<"Not a postive integer conversion.\n";
			return 0;
		}else{
			sum=sum*10+(s[i]-48);
			i++;
		}
	}
	return sum;
}

//function to accept integer numbers
void checkNode(int *nodeValue){
	//initilize string and int
	std::string userString;
	int integer;
	//while in loop ask for input and read the whole line
	while(std::getline(std::cin,userString)){
		//convert string to string stream,
		//therefore stream functions can be used
		std::stringstream strStream(userString);
		//convert to integer
		if(strStream>>integer){
			//check until end of file(line) is reached
			if(strStream.eof()){
				break;
			}
		}
		std::cout<<"Please enter an integer: \n";
	}
	//make the nodeValue into the userInput
	*nodeValue=integer;
}

